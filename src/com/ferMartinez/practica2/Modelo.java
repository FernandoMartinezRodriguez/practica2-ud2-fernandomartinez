package com.ferMartinez.practica2;

import java.sql.*;

public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Constructor de la clase, inicializa los ArrayLists,
     * carga los datos del fichero properties y setea isChanged a false.
     */
    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Carga los datos desde un fichero
     */
    private Connection conexion;

    void conectar() {

        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/mibase",user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    void insertarAutor(String nombre, String apellidos, String fechaNacimiento, String pais) {
        String sentenciaSql = "INSERT INTO autores (nombre, apellidos, fechanacimiento, pais) VALUES (?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, fechaNacimiento);
            sentencia.setString(4, pais);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void insertarEditorial(String editorial, String email, String telefono,  String web) {
        String sentenciaSql = "INSERT INTO editoriales (editorial, email, telefono, web) VALUES (?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, editorial);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, web);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void insertarLibro(String titulo, String isbn, String editorial, String genero, String autor,
                       float precio, LocalDate fechaLanzamiento) {
        String sentenciaSql = "INSERT INTO libros (titulo, isbn, ideditorial, genero, idautor, precio, fechalanzamiento) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int ideditorial = Integer.valueOf(editorial.split(" ")[0]);
        int idautor = Integer.valueOf(autor.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, titulo);
            sentencia.setString(2, isbn);
            sentencia.setInt(3, ideditorial);
            sentencia.setString(4, genero);
            sentencia.setInt(5, idautor);
            sentencia.setFloat(6, precio);
            sentencia.setDate(7, Date.valueOf(fechaLanzamiento));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarEditorial(String editorial, String email, String telefono, String web, int ideditorial){

        String sentenciaSql = "UPDATE editoriales SET editorial = ?, email = ?, telefono = ?, tipoeditorial = ?, web = ?" +
                "WHERE ideditorial = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, editorial);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, web);
            sentencia.setInt(5, ideditorial);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarAutor(String text, String nombre, String apellidos, String fechanacimiento, String pais, int idautor){

        String sentenciaSql = "UPDATE autores SET nombre = ?, apellidos = ?, fechanacimiento = ?, pais = ?" +
                "WHERE idautor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setString(3, fechanacimiento);
            sentencia.setString(4, pais);
            sentencia.setInt(5, idautor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarLibro(String titulo, String isbn, String editorial, String genero, String autor,
                        float precio, LocalDate fechalanzamiento, int idlibro) {

        String sentenciaSql = "UPDATE libros SET titulo = ?, isbn = ?, ideditorial = ?, genero = ?, " +
                "idautor = ?, precio = ?, fechalanzamiento = ? WHERE idlibro = ?";
        PreparedStatement sentencia = null;

        int ideditorial = Integer.valueOf(editorial.split(" ")[0]);
        int idautor = Integer.valueOf(autor.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, titulo);
            sentencia.setString(2, isbn);
            sentencia.setInt(3, ideditorial);
            sentencia.setString(4, genero);
            sentencia.setInt(5, idautor);
            sentencia.setFloat(6, precio);
            sentencia.setDate(7, Date.valueOf(fechalanzamiento));
            sentencia.setInt(8, idlibro);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Este metodo elimina una editorial
     * @param ideditorial id de la editorial
     */
    void borrarEditorial(int ideditorial) {
        String sentenciaSql = "DELETE FROM editoriales WHERE ideditorial = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, ideditorial);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Este metodo elimina un autor
     * @param idautor id del autor
     */
    void borrarAutor(int idautor) {
        String sentenciaSql = "DELETE FROM autores WHERE idautor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idautor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Este metodo elimina un libro
     * @param idlibro id del libro
     */
    void borrarLibro(int idlibro) {
        String sentenciaSql = "DELETE FROM libros WHERE idlibro = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idlibro);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Este metodo devuelve las editoriales guardadas en la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarEditorial() throws SQLException {
        String sentenciaSql = "SELECT concat(ideditorial) as 'ID', concat(editorial) as 'Nombre editorial', concat(email) as 'Email', " +
                "concat(telefono) as 'Teléfono', concat(tipoeditorial) as 'Tipo', concat(web) as 'Web' FROM editoriales";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo devuelve los autores guardados en la bbdd
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarAutor() throws SQLException {
        String sentenciaSql = "SELECT concat(idautor) as 'ID', concat(nombre) as 'Nombre', concat(apellidos) as 'Apellidos', " +
                "concat(fechanacimiento) as 'Fecha de nacimiento', concat(pais) as 'País de origen' FROM autores";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo devuelve todos los libros de la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarLibros() throws SQLException {
        String sentenciaSql = "SELECT concat(b.idlibro) as 'ID', concat(b.titulo) as 'Título', concat(b.isbn) as 'ISBN', " +
                "concat(e.ideditorial, ' - ', e.editorial) as 'Editorial', concat(b.genero) as 'Género', " +
                "concat(a.idautor, ' - ', a.apellidos, ', ', a.nombre) as 'Autor', " +
                "concat(b.precio) as 'Precio', concat(b.fechalanzamiento) as 'Fecha de publicación'" +
                " FROM libros as b " +
                "inner join editoriales as e on e.ideditorial = b.ideditorial inner join " +
                "autores as a on a.idautor = b.idautor";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Lee el archivo de propiedades y setea los atributos pertinentes
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param ip   ip de la bbdd
     * @param user user de la bbdd
     * @param pass contraseña de la bbdd
     * @param adminPass contraseña del administrador
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    /**
     * Este metodo comprueba si el isbn de un libro ya esta en la base de datos
     * @param isbn isbn del libro
     * @return true si ya existe
     */
    public boolean libroIsbnYaExiste(String nombre) {
        String salesConsult = "SELECT existeIsbn(?)";
        PreparedStatement function;
        boolean isbnExists = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            isbnExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isbnExists;
    }

    /**
     * Este metodo comprueba si el nombre de la editorial ya existe en la base de datos
     * @param nombre nombre de la editorial
     * @return true si ya existe
     */
    public boolean editorialNombreYaExiste(String nombre) {
        String editorialNameConsult = "SELECT existeNombreEditorial(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(editorialNameConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /**
     * Este metodo comprueba si el nombre completo de un autor
     * ya existe en la base de datos
     * @param nombre nombre del autor
     * @param apellidos apellido del autor
     * @return true si existe
     */
    public boolean autorNombreYaExiste(String nombre) {
        String completeName = nombre;
        String authorNameConsult = "SELECT existeNombrePrimero(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(authorNameConsult);
            function.setString(1, completeName);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }
}

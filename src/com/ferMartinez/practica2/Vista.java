package com.ferMartinez.practica2;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista<OptionDialog> extends JFrame{

    private final static String TITULOFRAME = "BBDDVariasTablas";
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    /*MENÚ*/
    JTextField txtNombreMenu;
    JTextField txtPostre;
    JComboBox comboPrimero;
    JComboBox comboSegundo;
    JTextField txtPrecio;
    JLabel lblNombreMenu;
    JLabel lblPrimeroMenu;
    JLabel lblSegundoMenu;
    JLabel lblPostre;
    JLabel lblPrecio;
    JLabel lblFecha;
    JTable table1;
    DateTimePicker fecha;
    JButton btnEliminar;
    JButton btnNuevo;
    /*PRIMERO*/
    JTextField txtNombrePrimero;
    JTextField txtIngredientePrimero;
    JTextField txtAcompañamientoPrimero;
    JTextField txtCantidadPrimero;
    JLabel lblNombrePrimero;
    JLabel lblIngredientePrimero;
    JLabel lblAcompañamientoPrimero;
    JLabel lblCantidadPrimero;
    JTable table2;
    JButton btnEliminarP;
    JButton btnNuevoP;
    /*SEGUNDO*/
    JTextField txtNombresegundo;
    JTextField txtIngredienteSegundo;
    JTextField txtAcompañamientoSegundo;
    JTextField txtCantidadSegundo;
    JLabel lblNombreSegundo;
    JLabel lblIngredientesegundo;
    JLabel lblAcompañamientoSegundo;
    JLabel lblCantidadSegundo;
    JTable table3;
    JButton btnEliminarS;
    JButton btnNuevoS;
    /*SEARCH*/
    JLabel lblAccion;
    JComboBox comboTipo;
    JLabel lblTipo;
    /*OPTION DIALOG*/
    OptionDialog optionDialog;
    /*DEFAULT TABLE MODELS*/
    DefaultTableModel dtmMenus;
    DefaultTableModel dtmPrimeros;
    DefaultTableModel dtmSegundos;
    /*MENUBAR*/
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;


    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    /**
     * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
     */
    private void setTableModels() {

        this.dtmMenus = new DefaultTableModel();
        this.table1.setModel(dtmMenus);

        this.dtmPrimeros = new DefaultTableModel();
        this.table2.setModel(dtmPrimeros);

        this.dtmSegundos = new DefaultTableModel();
        this.table3.setModel(dtmSegundos);
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        for(TiposPrimeros constant : TiposPrimeros.values()) { comboTipo.addItem(constant.getValor()); }
        comboTipo.setSelectedIndex(-1);
    }

    /**
     * Setea un JDialog para introducir la contreña de administrador y poder configurar la com.ferMartinez.practica2.base de datos
     */
    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }
}

package com.ferMartinez.practica2;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador implements ActionListener, TableModelListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Constructor de la clase, inicializa el modelo y la vista, carga los datos
     * desde un fichero, añade los listeners y lista los datos.
     *
     * @param modelo Modelo
     * @param vista  Vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    private void refrescarTodo() {
        refrescarPrimeros();
        refrescarSegundos();
        refrescarMenus();
        refrescar = false;
    }

    /**
     * Añade ActionListeners a los botones y menus de la vista
     * @param listener ActionListener que se añade
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnEliminar.addActionListener(listener);
        vista.btnEliminarP.addActionListener(listener);
        vista.btnEliminarS.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.btnNuevoP.addActionListener(listener);
        vista.btnNuevoS.addActionListener(listener);
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }

    /**
     * Añade WindowListeners a la vista
     * @param listener WindowListener que se añade
     */
    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     * @param e Evento producido en una lista
     */

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.table3.getSelectionModel())) {
                int row = vista.table3.getSelectedRow();
                vista.txtNombresegundo.setText(String.valueOf(vista.table3.getValueAt(row, 1)));
                vista.txtIngredienteSegundo.setText(String.valueOf(vista.table3.getValueAt(row, 2)));
                vista.txtAcompañamientoSegundo.setText(String.valueOf(vista.table3.getValueAt(row, 3)));
                vista.txtCantidadSegundo.setText(String.valueOf(vista.table3.getValueAt(row, 4)));
            } else if (e.getSource().equals(vista.table2.getSelectionModel())) {
                int row = vista.table2.getSelectedRow();
                vista.txtNombrePrimero.setText(String.valueOf(vista.table2.getValueAt(row, 1)));
                vista.txtIngredientePrimero.setText(String.valueOf(vista.table2.getValueAt(row, 2)));
                vista.txtAcompañamientoPrimero.setText(String.valueOf(vista.table2.getValueAt(row, 3)));
                vista.txtCantidadPrimero.setText(String.valueOf(vista.table2.getValueAt(row, 4)));
            } else if (e.getSource().equals(vista.table1.getSelectionModel())) {
                int row = vista.table1.getSelectedRow();
                vista.txtNombreMenu.setText(String.valueOf(vista.table1.getValueAt(row, 1)));
                vista.comboPrimero.setSelectedItem(String.valueOf(vista.table1.getValueAt(row, 5)));
                vista.comboSegundo.setSelectedItem(String.valueOf(vista.table1.getValueAt(row, 3)));
                vista.txtPostre.setText(String.valueOf(vista.table1.getValueAt(row, 2)));
                vista.fecha.setDate((Date.valueOf(String.valueOf(vista.table1.getValueAt(row, 6)))).toLocalDate());
                vista.txtPrecio.setText(String.valueOf(vista.table1.getValueAt(row, 4)));
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.table3.getSelectionModel())) {
                    borrarCamposSegundos();
                } else if (e.getSource().equals(vista.table2.getSelectionModel())) {
                    borrarCamposPrimeros();
                } else if (e.getSource().equals(vista.table1.getSelectionModel())) {
                    borrarCamposMenus();
                }
            }
        }
    }
    /**
     * Añade sus respectivas acciones a los botones de añadir, modificar y eliinar de las listas.
     * Añade sus respectivas acciones a los menus de guardar, guardar como y mostrar opciones.
     * Añade la acción de guardar las opciones al dialog de opciones.
     * Añade las acciones de guardar, no guardar y cancelar a los botones del dialog de guardar
     *  cambios cuando se intenta salir de la aplicación una vez se ha desactivado el autoguardado.
     * @param e Evento producido al pulsar un botón o menu
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if(String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(), vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()), String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "anadirLibro": {
                try {
                    if (comprobarMenuVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table1.clearSelection();
                    } else if (modelo.libroIsbnYaExiste(vista.txtNombreMenu.getText())) {
                        Util.showErrorAlert("Ese ISBN ya existe.\nIntroduce un libro diferente");
                        vista.table1.clearSelection();
                    } else {
                        modelo.insertarLibro(
                                vista.txtNombreMenu.getText(),
                                vista.txtPostre.getText(),
                                String.valueOf(vista.comboPrimero.getSelectedItem()),
                                String.valueOf(vista.comboSegundo.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fecha.getDate());
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table1.clearSelection();
                }
                borrarCamposMenus();
                refrescarMenus();
            }
            break;
            case "modificarLibro": {
                try {
                    if (comprobarMenuVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table1.clearSelection();
                    } else {
                        modelo.modificarLibro(
                                vista.txtNombreMenu.getText(),
                                vista.txtPostre.getText(),
                                String.valueOf(vista.comboPrimero.getSelectedItem()),
                                String.valueOf(vista.comboSegundo.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecio.getText()),
                                vista.fecha.getDate(),
                                Integer.parseInt((String)vista.table1.getValueAt(vista.table1.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table1.clearSelection();
                }
                borrarCamposMenus();
                refrescarMenus();
            }
            break;
            case "eliminarLibro":
                modelo.borrarLibro(Integer.parseInt((String)vista.table1.getValueAt(vista.table1.getSelectedRow(), 0)));
                borrarCamposMenus();
                refrescarMenus();
                break;
            case "anadirAutor": {
                try {
                    if (comprobarPrimeroVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table2.clearSelection();
                    } else if (modelo.autorNombreYaExiste(vista.txtNombrePrimero.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un plato diferente");
                        vista.table2.clearSelection();
                    } else {
                        modelo.insertarAutor(vista.txtNombrePrimero.getText(),
                                vista.txtIngredientePrimero.getText(),
                                vista.txtAcompañamientoPrimero.getText(),
                                vista.txtCantidadPrimero.getText(),(String) vista.comboTipo.getSelectedItem(),);
                        refrescarPrimeros();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table2.clearSelection();
                }
                borrarCamposPrimeros();
            }
            break;
            case "modificarAutor": {
                try {
                    if (comprobarPrimeroVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table2.clearSelection();
                    } else {
                        modelo.modificarAutor(vista.txtNombrePrimero.getText(), vista.txtIngredientePrimero.getText(),
                                vista.txtAcompañamientoPrimero.getText(), vista.txtCantidadPrimero.getText(),String.valueOf(vista.comboTipo.getSelectedItem()),
                                Integer.parseInt((String)vista.table2.getValueAt(vista.table2.getSelectedRow(), 0)));
                        refrescarPrimeros();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table2.clearSelection();
                }
                borrarCamposPrimeros();
            }
            break;
            case "eliminarAutor":
                modelo.borrarAutor(Integer.parseInt((String)vista.table2.getValueAt(vista.table2.getSelectedRow(), 0)));
                borrarCamposPrimeros();
                refrescarPrimeros();
                break;
            case "anadirEditorial": {
                try {
                    if (comprobarSegundoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table3.clearSelection();
                    } else if (modelo.editorialNombreYaExiste(vista.txtNombresegundo.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce una editorial diferente.");
                        vista.table3.clearSelection();
                    } else {
                        modelo.insertarEditorial(vista.txtNombresegundo.getText(), vista.txtIngredienteSegundo.getText(),
                                vista.txtAcompañamientoSegundo.getText(),
                                vista.txtCantidadSegundo.getText());
                        refrescarSegundos();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table3.clearSelection();
                }
                borrarCamposSegundos();
            }
            break;
            case "modificarEditorial": {
                try {
                    if (comprobarSegundoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table3.clearSelection();
                    } else {
                        modelo.modificarEditorial(vista.txtNombresegundo.getText(), vista.txtIngredienteSegundo.getText(),
                                vista.txtAcompañamientoSegundo.getText(), vista.txtCantidadSegundo.getText(),
                                Integer.parseInt((String)vista.table3.getValueAt(vista.table3.getSelectedRow(), 0)));
                        refrescarSegundos();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table3.clearSelection();
                }
                borrarCamposSegundos();
            }
            break;
            case "eliminarEditorial":
                modelo.borrarEditorial(Integer.parseInt((String)vista.table3.getValueAt(vista.table3.getSelectedRow(), 0)));
                borrarCamposSegundos();
                refrescarSegundos();
                break;
        }
    }

    /**
     * Añade acciones cuando la ventana se cierra. Si el autoguardado está activado la ventana se cerrará
     * sin más, volcando todos los datos en el fichero. De lo contrario pueden ocurrir dos cosas:
     * Si todos los cambios han sido guardados, la ventana se cerrará. Si no lo están, aparecerá una ventana
     * que permitirá al usuario decidir que hacer con esos cambios no guardados o bien cancelar el cierre
     * de la aplicación.
     * @param e Evento producido al tratar de cerrar la ventana
     */
    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }
    /**
     * Actualiza las editoriales que se ven en la lista y los comboboxes
     */
    private void refrescarPrimeros() {
        try {
            vista.table2.setModel(construirTableModelPrimeros(modelo.consultarEditorial()));
            vista.comboPrimero.removeAllItems();
            for(int i = 0; i < vista.dtmPrimeros.getRowCount(); i++) {
                vista.comboPrimero.addItem(vista.dtmPrimeros.getValueAt(i, 0)+" - "+
                        vista.dtmPrimeros.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelPrimeros(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmPrimeros.setDataVector(data, columnNames);

        return vista.dtmPrimeros;

    }

    private void refrescarSegundos() {
        try {
            vista.table3.setModel(construirTableModeloSegundos(modelo.consultarAutor()));
            vista.comboSegundo.removeAllItems();
            for(int i = 0; i < vista.dtmSegundos.getRowCount(); i++) {
                vista.comboSegundo.addItem(vista.dtmSegundos.getValueAt(i, 0)+" - "+
                        vista.dtmSegundos.getValueAt(i, 2)+", "+vista.dtmSegundos.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloSegundos(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmSegundos.setDataVector(data, columnNames);

        return vista.dtmSegundos;

    }

    /**
     * Actualiza los libros que se ven en la lista y los comboboxes
     */
    private void refrescarMenus() {
        try {
            vista.table1.setModel(construirTableModelMenus(modelo.consultarLibros()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelMenus(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmMenus.setDataVector(data, columnNames);

        return vista.dtmMenus;

    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Setea el dialog de opciones según las opciones que el usuario guardo en la
     * última ejecución del programa.
     */
    private void setOptions() {
        vista.optionDialog.tfIP.setText(modelo.getIP());
        vista.optionDialog.tfUser.setText(modelo.getUser());
        vista.optionDialog.pfPass.setText(modelo.getPassword());
        vista.optionDialog.pfAdmin.setText(modelo.getAdminPassword());
    }

    /**
     * Vacía los campos de la tab de libros
     */
    private void borrarCamposMenus() {
        vista.comboPrimero.setSelectedIndex(-1);
        vista.comboSegundo.setSelectedIndex(-1);
        vista.txtNombreMenu.setText("");
        vista.txtPostre.setText("");
        vista.txtPrecio.setText("");
        vista.fecha.setText("");
    }

    /**
     * Vacía los campos de la tab de autores
     */
    private void borrarCamposPrimeros() {
        vista.txtNombrePrimero.setText("");
        vista.txtAcompañamientoPrimero.setText("");
        vista.txtIngredientePrimero.setText("");
        vista.txtCantidadPrimero.setText("");
        vista.comboTipo.setSelectedIndex(-1);
    }

    /**
     * Vacía los campos de la tab de editoriales
     */
    private void borrarCamposSegundos() {
        vista.txtNombresegundo.setText("");
        vista.txtIngredienteSegundo.setText("");
        vista.txtAcompañamientoSegundo.setText("");
        vista.txtCantidadSegundo.setText("");
    }

    /**
     * Comprueba que los campos necesarios para añadir un libro estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarMenuVacio() {
        return vista.txtNombreMenu.getText().isEmpty() ||
                vista.txtPrecio.getText().isEmpty() ||
                vista.txtPostre.getText().isEmpty() ||
                vista.comboSegundo.getSelectedIndex() == -1 ||
                vista.comboPrimero.getSelectedIndex() == -1 ||
                vista.fecha.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir un autor estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarPrimeroVacio() {
        return vista.txtNombrePrimero.getText().isEmpty() ||
                vista.txtIngredientePrimero.getText().isEmpty() ||
                vista.txtAcompañamientoPrimero.getText().isEmpty() ||
                vista.comboTipo.getSelectedIndex() == -1 ||
                vista.txtCantidadPrimero.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir una editorial estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarSegundoVacio() {
        return vista.txtNombresegundo.getText().isEmpty() ||
                vista.txtIngredienteSegundo.getText().isEmpty() ||
                vista.txtAcompañamientoSegundo.getText().isEmpty() ||
                vista.txtCantidadSegundo.getText().isEmpty();
    }

    /*LISTENERS IPLEMENTOS NO UTILIZADOS*/

    private void addItemListeners(Controlador controlador) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void tableChanged(TableModelEvent e) {

    }
}
